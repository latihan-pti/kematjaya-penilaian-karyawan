<?php

namespace Kematjaya\PenilaianKaryawan\Tests\Karyawan;

use Kematjaya\PenilaianKaryawan\Entity\Programmer;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class FrontendProgrammer extends Programmer 
{
    
    public function getProjectActive(): string 
    {
        return "Eproc";
    }

    public function getTools(): array 
    {
        return [
            'PHP Strom', 'React Native', 'Web Browser'
        ];
    }

    public function kerja(): string 
    {
        return "mulai kerja jam 9 sampai jam 5";
    }

}
