<?php

namespace Kematjaya\PenilaianKaryawan\Tests\Karyawan;

use Kematjaya\PenilaianKaryawan\Entity\Programmer;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class BackendProgrammer extends Programmer
{
    
    public function getProjectActive(): string 
    {
        return "Eproc";
    }

    public function getTools(): array 
    {
        return [
            'Netbeans', 'PHP', 'Web Browser'
        ];
    }

    public function kerja(): string 
    {
        return "mulai kerja jam 9 sampai jam 5";
    }

}
