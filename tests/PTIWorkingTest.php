<?php

namespace Kematjaya\PenilaianKaryawan\Tests;

use Kematjaya\PenilaianKaryawan\Tests\Karyawan\BackendProgrammer;
use Kematjaya\PenilaianKaryawan\Tests\Karyawan\FrontendProgrammer;
use Kematjaya\PenilaianKaryawan\PTIWorking;
use PHPUnit\Framework\TestCase;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PTIWorkingTest extends TestCase
{
    public function testKerja()
    {
        $working = new PTIWorking();
        $this->assertEmpty($working->getKaryawans());
        
        $derta = new FrontendProgrammer("derta");
        $agung = new BackendProgrammer("agung");
        
        $working->addKaryawan($derta);
        $working->addKaryawan($agung);
        
        $this->assertEquals(2, count($working->getKaryawans()));
        
        $this->expectOutputRegex('/' . implode(", ", $derta->getTools()) . '/');
        $working->mulaiKerja();
    }
}
