<?php

namespace Kematjaya\PenilaianKaryawan\Entity;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class Programmer extends Karyawan
{
    abstract public function getProjectActive():string; 
}
