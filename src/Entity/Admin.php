<?php

namespace Kematjaya\PenilaianKaryawan\Entity;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class Admin extends Karyawan
{
    abstract public function getJobDesk():string;
    
    public function mulaiKerja():string
    {
        return $this->name . 'kerja mengerjakan '.$this->getJobDesk() .' menggunakan tool: '. implode(", ", $this->getTools());
    }
}
