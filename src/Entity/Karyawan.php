<?php

namespace Kematjaya\PenilaianKaryawan\Entity;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class Karyawan implements KaryawanInterface
{
    protected $name;
    
    public function __construct(string $name) 
    {
        $this->name = $name;
    }
    
    public function mulaiKerja():string
    {
        return $this->name . " " .$this->kerja() . ' menggunakan tool: '. implode(", ", $this->getTools()) . '<br/>';
    }
    
    abstract public function getTools():array;
}
