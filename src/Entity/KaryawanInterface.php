<?php

namespace Kematjaya\PenilaianKaryawan\Entity;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface KaryawanInterface 
{
    public function kerja():string;
}
