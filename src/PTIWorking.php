<?php

namespace Kematjaya\PenilaianKaryawan;

use Kematjaya\PenilaianKaryawan\Entity\Karyawan;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PTIWorking 
{
    private $karyawans;
    
    public function __construct() 
    {
        $this->karyawans = array();
    }
    
    public function addKaryawan(Karyawan $karyawan)
    {
        $this->karyawans[] = $karyawan;
    }
    
    public function getKaryawans():array
    {
        return $this->karyawans;
    }
    
    public function mulaiKerja()
    {
        foreach($this->karyawans as $karyawan)
        {
            echo $karyawan->mulaiKerja();
        }
    }
}
